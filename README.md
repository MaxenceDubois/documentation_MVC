# L'architecture Modèle-Vue-Contrôleur

Le Modèle-Vue-contrôleur ou M-V-C est modèle architectural offrant un cadre normalisé pour structurer une application, elle facilite aussi le dialogue entre les concepteurs. Il sépare une application en trois couches distinctes :

## le modèle

Le modèle contient toute la logique en relation avec les données que manipule l’utilisateur. Cela peut être les données qui transitent entre la vue et le contrôleur ou les données envoyées par l’utilisateur. Cette couche contient la majeure partie de la logique de l’application.

Exemple 1 : un modèle chargé de la gestion client va retrouver les informations d’un client dans la base de données, ajouter ou retirer des données et mettre à jour la base de données.

Exemple 2 : dans le cas d’un réseau social, la couche Model s’occupe des tâches comme de sauvegarder des données, de sauvegarder des associations d’amis, d’enregistrer et de récupérer les photos des utilisateurs, de trouver des suggestions de nouveaux amis, etc ... Tandis que les objets Models seront “Ami”, “User”, “Commentaire”, “Photo”.

## la vue

La vue contient toutes les informations graphiques d’une application. Cela peut aller du graphisme (barre de navigation, images, icônes, vidéos, musiques…) au texte devant être montré à l’utilisateur de l’application (descriptions, chiffres, formulaires, cases à cocher…).

Exemple 1 : une vue chargée de la gestion client va afficher les informations d’un client ainsi qu'un formulaire pour ajouter les achats de ce dernier.

Exemple 2 : pour un réseau social, la vue s’occupe d'afficher toutes les informations retournées par le modèle tel que les photos, les status, les liens, les vidéos...

## le contrôleur

Le contrôleur est le maître d’orchestre faisant la liaison entre le modèle chargé de toute la logique et la vue chargée de rendre les informations à l’utilisateur de l’application. Il gère les requêtes des utilisateurs en envoyant ces dernières vers le bon modèle (en vérifiant leur validité au passage).

Exemple 1 : le contrôleur client se chargera de transmettre les informations saisies par l’utilisateur au modèle client qui se chargera de les traiter. Il se chargera d’autre part de renvoyer toutes les informations données par le modèle à la vue.

Exemple 2 : dans le cas d’un réseau social, le contrôleur récupère les informations saisies par l'utilisateur (status, vidéos, photos...) pour permettre au modèle de les enregistrer sur la table de l'utilisateur.


Le M-V-C est le schéma de développement le plus utilisé en entreprise pour la création de projets évolutifs et extensibles. Chacun de ces trois composants concerne un aspect du développement logiciel, cela permet un partage des tâches pertinent entre les équipes de développement et réduit les conflits lors des fusions sur GIT.
